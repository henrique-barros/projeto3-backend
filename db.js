'use strict';

//Import the mongoose module
var mongoose = require('mongoose');

//Change for localhost or mongo depending if you are running the project on docker or local
var mongoDB = 'mongodb://localhost:27017/db';

mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = db;
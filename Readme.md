quando for executar o docker, no arquivo './db.js' alterar a URL da conexão do banco de dados de localhost para mongo

para logar na máquina do docker:
- docker exec -it nodeapp_code_1 /bin/bash


Post análise:
Parâmetors:
- name: nome da análise
- collection_name: nome da coleção em que será armazenados os dados da análise
- description: descrição da análise

```
curl -X POST \
  http://localhost:8000/analysis \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: c05607e6-73e8-96a2-9498-c3e98ba520ca' \
  -d '{
  "name": "Dois",
  "collection_name": "dois",
  "description": "Testing analysis api" 
}'
```


Post dados:
Parâmetors:
- collection: nome da coleção que tem os dados da análise
- data: json contendo os dados da análise

```
curl -X POST \
  http://localhost:8000/data \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: a5b3329a-482e-09ff-8cb5-38fc6b6da49d' \
  -d '{
  "collection": "transito",
  "data": {
    "velocidade": "10",
    "umidade": "90"
  }
}'
```


Get dados:
Parâmetros:
- collection: nome da coleção que quer coletar os dados

Retorna:
- array dos dados da análise

```
curl -X GET \
  'http://localhost:8000/data?collection=transito' \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 4660a057-9533-7a95-5c2c-758940021336'
```


Get análises:
Retorna:
- array de análises e suas descrições

```
curl -X GET \
  http://localhost:8000/analysis \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 5cf6bcdf-8791-ef87-2eec-10923d8ad3cb' \
  -d '{
  name: "Ghost",
  collection_name: "ghost",
  description: "Testing analysis api" 
}'
```
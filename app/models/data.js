var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dataSchema = new Schema({}, {strict: false})

const getModel = (name) => {
  return mongoose.model(name, dataSchema)
}

module.exports = { getModel };
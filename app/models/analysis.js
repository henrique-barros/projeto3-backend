var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnalysisSchema = new Schema({ name: String, collection_name: String, description: String });

module.exports = mongoose.model('Analysis', AnalysisSchema);
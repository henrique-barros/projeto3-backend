var express = require('express')
var router = express.Router()
import { getAnalyses, saveAnalysis } from '../controllers/analysis'

router.get('/', (req, res) => {
  getAnalyses(
    (analysis) => {
      res.json(analysis)
    },
    (error) => {
      res.json({error_message: "Erro"})
    }
  )
})

router.post('/', (req, res) => {
  saveAnalysis(req.body,
    () => {
      res.json({"ok": true})
    },
    () => {
      res.json("ok", false)
    }
  )
})

module.exports = router
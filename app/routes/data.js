var express = require('express')
var router = express.Router()
import { getData, saveData, bulkSaveData } from '../controllers/data'

router.get('/', (req, res) => {
  getData(
    req.query.collection,
    req.query.date,
    (data) => {
      res.json(data)
    },
    (error) => {
      res.json({error_message: "Erro"})
    }
  )
})

router.post('/', (req, res) => {
  saveData(req.body.collection, req.body.data,
    () => {
      res.json({"ok": true})
    },
    () => {
      res.json("ok", false)
    }
  )
})

router.post('/bulk', (req, res) => {
  bulkSaveData(req.body.collection, req.body.data,
    () => {
      res.json({"ok": true})
    },
    () => {
      res.json("ok", false)
    }
  )
})


module.exports = router
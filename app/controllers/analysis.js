'use strict';

import Analysis from '../models/analysis';

/**
 * Module dependencies.
 */


const getAnalyses = (callbackSuccess, callbackError) => {
  Analysis.find({}, 'name collection_name description', function (err, analyses) {
    if (err) {
      callbackError(err);
      return;
    }
    callbackSuccess(analyses);
  });
}

const saveAnalysis = (analysis, callbackSuccess, callbackError) => { 
  var newAnalysis = new Analysis(analysis);
  newAnalysis.save(function (err, analysis) {
    if (err) {
      callbackError(err);
      return;
    }
    else {
      callbackSuccess();
    }
  });
}

module.exports = {
  getAnalyses,
  saveAnalysis
}
'use strict';

/**
 * Module dependencies.
 */

 import { getModel } from '../models/data'


const getData = (collectionName, date,callbackSuccess, callbackError) => {
  const Model = getModel(collectionName)
  if (date) {
    Model.find({date: date}, function (err, analyses) {
      if (err) {
        callbackError(err);
        return;
      }
      callbackSuccess(analyses);
    });
  }
  else {
    Model.find({}, function (err, analyses) {
      if (err) {
        callbackError(err);
        return;
      }
      callbackSuccess(analyses);
    });
  }
}

const saveData = (collectionName, data, callbackSuccess, callbackError) => { 
  const Model = getModel(collectionName)
  var newData = new Model(data);
  newData.save(function (err, data) {
    if (err) {
      callbackError(err);
      return;
    }
    else {
      callbackSuccess();
    }
  });
}

const bulkSaveData = (collectionName, data, callbackSuccess, callbackError) => { 
  const Model = getModel(collectionName);
  Model.insertMany(data ,function (err, data) {
    if (err) {
      callbackError(err);
      return;
    }
    else {
      callbackSuccess();
    }
  });
}

module.exports = {
  getData,
  saveData,
  bulkSaveData
}
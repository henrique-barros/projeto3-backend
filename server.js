const express = require('express');
const server = express();
const PORT = 8000;
var bodyParser = require('body-parser');

import db from './db';
import analysisRoutes from './app/routes/analysis'
import dataRoutes from './app/routes/data'

// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
server.use(bodyParser.json())

server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

server.listen(PORT, () => console.log(`Server running on ${PORT}`));

server.use('/analysis', analysisRoutes)
server.use('/data', dataRoutes)

server.get('/', 
(req, res) => 
  res.status(200).send('hello')
);

server.post('/data',
  (req, res) => {
    res.setHeader('Content-Type', 'text/plain')
    res.write('you posted:\n')
    res.end(JSON.stringify(req.body, null, 2))
});
FROM node:7
RUN mkdir /code
ADD . /code
WORKDIR /code
COPY package.json /code
RUN npm i
CMD ["npm", "start"]